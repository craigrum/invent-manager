class Item:
    def __init__(self,line):
        import re
        #Pull content from line with regex
        try:
            #Regex for parsing  (               G1                 ) ( G5  ) (  G6 )
            regex = re.compile("((([A-Z]|[a-z])+\s*)+([A-Z]|[a-z])*) (-?\d+) (-?\d+)")
            groups=regex.search(line) 
            self.name = groups.group(1)
            self.key = self.name.lower().replace(" ","")
            self.sellin = int(groups.group(5))
            self.value = int(groups.group(6))
            self.valid = self.isValid()
        #If the regex fails then or the item name is not found then valid is set to false
        except Exception as ex:
            print str(ex)
            self.valid = False

    #Returns the string representation for valid and invalid items
    def __str__(self):
        output = self.name + " " + str(self.sellin) + " " + str(self.value) if self.valid else "NO SUCH ITEM"
        return output

    #Checks the item name is valid
    def isValid(self):
        return self.key in ['agedbrie','backstagepasses', 'sulfuras','normalitem','conjured']

    #Calculates the new values for a given day and stores them as the items new values
    def nextDay(self):
        self.value = self.ruleSwitch()
        if not self.key == 'sulfuras': 
            self.sellin = self.sellin - 1

    def bireRule(self):
        return self.value + 1 if not self.value is 50 else self.value

    def backStagePassesRule(self):
        new_value = self.value + 1
        if 10 > self.sellin > 5:
            new_value = self.value + 2
        if self.sellin <= 5:
            new_value = self.value + 3
        if 0 >= self.sellin:
            new_value = 0
        return new_value if not new_value > 50 else 50 

    def sulfurasRule(self):
        return self.value

    def normalitemRule(self):
        return self.overFiftyCorrector(self.value - 1 if self.sellin > 0 else self.value - 2)

    def conjuredRule(self):
        return self.overFiftyCorrector(self.value - 2 if self.sellin > 0 else self.value - 4)

    def overFiftyCorrector(self,value):
        return 50 if value > 50 else value

    def ruleSwitch(self):
        if self.valid:
            return {
                'agedbrie': self.bireRule(),
                'backstagepasses': self.backStagePassesRule(),
                'sulfuras': self.sulfurasRule(),
                'normalitem': self.normalitemRule(),
                'conjured': self.conjuredRule()
                }[self.key]
        return 0
