class InventManager:
        #initialization
        def __init__(self):
                #Declare Item List
                from item import Item
                self.items = []
                #Read Input File
                input_reader = open("input.txt","r")
                for line in input_reader:
                        #Construct item object from line
                        self.items.append(Item(line))

        def printItems(self):
                for item in self.items:
                        print str(item)
                print ""

        def nextDay(self):
                for item in self.items:
                        item.nextDay()
