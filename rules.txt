{
    Reduction:1,
    OODModifier:2,
    Negitives:false,
    BrieModifier:-1,
    MaxQuality:50,
    SulfurasModifier:0,
    BackStagePassRule:["x<10 & x>5:2","x<5:3","x<=0:y=0"],
    ConjuresModifer:2
}

All items have a SellIn value which denotes the number of days we have to sell the
item
● All items have a Quality value which denotes how valuable the item is
● At the end of each day our system lowers both values for every item
● Once the sell by date has passed, Quality degrades twice as fast
● The Quality of an item is never negative
● "Aged Brie" actually increases in Quality the older it gets
● “Normal Item” decreases in Quality by 1
● The Quality of an item is never more than 50
● "Sulfuras", being a legendary item, never has to be sold or decreases in Quality
● "Backstage passes", like aged brie, increases in Quality as its SellIn value
approaches; Its quality increases by 2 when there are 10 days or less and by 3 when
there are 5 days or less but quality drops to 0 after the concert
● "Conjured" items degrade in Quality twice as fast as normal items